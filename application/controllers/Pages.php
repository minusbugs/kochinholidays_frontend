<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    
     public function __construct() {
    //load database in autoload libraries 
        parent::__construct(); 
        $this->load->helper('url_helper');
        $this->load->library('session'); 
        $this->load->helper('form'); 
        $this->load->model('DestinationModel');         
        $this->load->model('PackageModel');   
        $this->load->model('AirportModel');   
        $this->load->model('HotelModel'); 
        $this->load->model('HomeSliderModel');        
   }
    public function index()
    {
        
        $data['main_title']="Kochin Holidays -Kerala Tourism ";
        $data['title_name']="home";
        $data['page_name']="Home";
        $data['all_airport']=$this->AirportModel->get_all_airport();
        $data['all_hotel']=$this->HotelModel->get_all_hotel();
        $data['destinations_details'] = $this->DestinationModel->get_destinations();
        $data['packages_details'] =$this->PackageModel->get_all_latest_packages();
        $data['home_slider'] =$this->HomeSliderModel->get_all_sliders();
        $this->load->view('pages/home_page',$data);
    
    }

    public function packages()
    {
        
        $data = array('content'=>'pages/packages_page',
                      'msg'=>'Pacakagesssssssssss',
                      'main_title'=>'Kochin Holidays -Kerala Tour Package | Package Tour Kerala | Kerala Tourism Package ',
                      'title_name'=>'packages',
                      'page_name'=>'Packages',
                      'packages_details' =>$this->PackageModel->get_all_packages(),
                      );
        $this->load->view('includes/template',$data);
    }
    

    public function destinations()
    {
        $data = array('content'=>'pages/destinations_page',
                      'title_name'=>'destination',
                      'main_title'=>'Kochin Holidays -Kerala Tour Place | Destination Tour Kerala | Kerala Tourism Destination ',
                      'page_name'=>'Destinations',
                      'destinations_details'=>$this->DestinationModel->get_destinations(),
                      'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }

    public function about()
    {
        $data = array('content'=>'pages/about_page',
                      'title_name'=>'aboutus',
                      'page_name'=>'About Us',
                      'main_title'=>'Kochin Holidays -About Page',
                      'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }

    public function reachus()
    {
        $data = array('content'=>'pages/reachus_page',
                      'title_name'=>'reachus',
                      'page_name'=>'Contact Us',                      
                      'main_title'=>'Kochin Holidays -Contact Us Page',
                      'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }
    public function terms()
    {
        $data = array('content'=>'pages/terms_page',
                        'title_name'=>'',
                        'page_name'=>'Terms',
                        'main_title'=>'Kochin Holidays -Terms Page',
                        'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }
    public function privacy()
    {
        $data = array('content'=>'pages/privacy_page',
                      'title_name'=>'',
                      'page_name'=>'Privacy',
                      'main_title'=>'Kochin Holidays -Privacy Page ',
                      'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }
    

    

    public function PackageDeatils($id){

        $data = array('content'=>'pages/package_book_page',
                      'title_name'=>'',
                      'page_name'=>'Package Booking',
                      'main_title'=>'Kochin Holidays -Package Booking Page ',
                      'packages_details_by_id' =>$this->PackageModel->get_packages_by_id($id),
                      'packages_details' =>$this->PackageModel->get_all_packages());
        $this->load->view('includes/template',$data);
    }

    public function DestinationDetails($id){
        
        $destination = $this->DestinationModel->get_destination_id($id);
        $data = array('content'=>'pages/packages_by_destination_page',
                    'msg'=>'Pacakagesssssssssss',
                    'title_name'=>'',
                    'page_name'=>'Package Details',
                    'main_title'=>'Kochin Holidays -Packages',
                    'packages_details' =>$this->PackageModel->get_all_packages_by_destinaionid($id),
                    'destination_name'=>$destination->DestinationName,
                    );
        // print_r($this->PackageModel->get_all_packages_by_destinaionid($id));exit;
        $this->load->view('includes/template',$data);
    }

        public function send_mail() 
        { 
             $content=$this->input->post('content');
             $fullName=$this->input->post('name');
             $from_email=$this->input->post('email');
             $message='<html><body>';
             $message.='<img src="http://kochinholidays.co.in/QA/images/logo.png" alt="Kochin Holidays" />';
             $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
             $message .= "<tr style='background: #eee;'><td><strong>Full Name:</strong> </td><td>" . $fullName . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Email :</strong> </td><td>" . strip_tags($from_email) . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Message :</strong> </td><td>" .$content. "</td></tr>";
             $message .= "</table>";
             $message .= "</body></html>";
             $config['protocol'] = "smtp";
             $config['smtp_host'] = "ssl://smtp.gmail.com";
             $config['smtp_port'] = "465";
             $config['smtp_user'] = smtp_user; 
             $config['smtp_pass'] = smtp_pass;
             $config['newline'] = "\r\n";
            //  Load email library
            $this->load->library('email');
            $config['charset']  = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);
            $this->email->from($from_email, 'Kochin Holidays');
            $this->email->to(to_email);
            $this->email->subject('Feedback from Kochin Holidays');
            $this->email->message($message);
         if($this->email->send())
         {
                $result='1';
                echo json_encode($result);
         } 
         else
         {
               $error = '0';
                echo json_encode($error);
         }
      }

      public function send_flight_mail()
      {
            $FlightFrom=$this->input->post('selectFlightFrom');
            $FlightTo=$this->input->post('selectFlightTo');
            $Departing=$this->input->post('dateDeparting');
            $ContactNumber=$this->input->post('txtContactNumber');
            $Adult=$this->input->post('selectAdult');
            $Child=$this->input->post('selectChild');
            $from_email=smtp_user;
     
             $message='<html><body>';
             $message.='<img src="http://kochinholidays.co.in/QA/images/logo.png" alt="Kochin Holidays" />';
             $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
             $message .= "<tr style='background: #eee;'><td><strong>Flight From :</strong> </td><td>" . $FlightFrom. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Flight To :</strong> </td><td>" . $FlightTo . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Departing :</strong> </td><td>" .$Departing. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Contact Number:</strong> </td><td>" .$ContactNumber. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Adult Count:</strong> </td><td>" .$Adult. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Child Count:</strong> </td><td>" .$Child. "</td></tr>";
             $message .= "</table>";
             $message .= "</body></html>";

             $config['protocol'] = "smtp";
             $config['smtp_host'] = "ssl://smtp.gmail.com";
             $config['smtp_port'] = "465";
             $config['smtp_user'] = smtp_user; 
             $config['smtp_pass'] = smtp_pass;
             $config['newline'] = "\r\n";
            //  Load email library
            $this->load->library('email');
            $config['charset']  = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from($from_email, 'Kochin Holidays');
            $this->email->to(to_email);
            $this->email->subject('Flight Request Message');
            $this->email->message($message);
            if($this->email->send())
            {
                $result='1';
                echo json_encode($result);
            } 
            else
           {
                $error = '0';
                echo json_encode($error);
           }
      }

      public function send_hotel_mail()
      {
            $HotelName=$this->input->post('selectHotelName');
            $CheckIn=$this->input->post('dateCheckIn');
            $ChechOut=$this->input->post('dateChechOut');
            $ContactNumber=$this->input->post('txtContactNumber');
            $from_email=smtp_user;
          
             $message='<html><body>';
             $message.='<img src="http://kochinholidays.co.in/QA/images/logo.png" alt="Kochin Holidays" />';
             $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
             $message .= "<tr style='background: #eee;'><td><strong>Hotel Name :</strong> </td><td>" . $HotelName. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Check In :</strong> </td><td>" . $CheckIn. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Chech Out:</strong> </td><td>" .$ChechOut. "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Contact Number:</strong> </td><td>" .$ContactNumber. "</td></tr>";
             $message .= "</table>";
             $message .= "</body></html>";

             $config['protocol'] = "smtp";
             $config['smtp_host'] = "ssl://smtp.gmail.com";
             $config['smtp_port'] = "465";
             $config['smtp_user'] = smtp_user; 
             $config['smtp_pass'] = smtp_pass;
             $config['newline'] = "\r\n";
            //  Load email library
            $this->load->library('email');
            $config['charset']  = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from($from_email, 'Kochin Holidays');
            $this->email->to(to_email);
            $this->email->subject('Hotel Request Message');
            $this->email->message($message);
            if($this->email->send())
            {
                $result='1';
                echo json_encode($result);
            } 
            else
           {
                $error = '0';
                echo json_encode($error);
           }
      }

    public function send_Package_Details() 
        { 
             $firstName=$this->input->post('txtFirtsName');
             $lastName=$this->input->post('txtLastName');
             $packageName=$this->input->post('hd_packagename');
             $from_email=$this->input->post('txtEmail');
             $mobileNumber=$this->input->post('txtMobileNo');
             $message='<html><body>';
             $message.='<img src="http://kochinholidays.co.in/QA/images/logo.png" alt="Kochin Holidays" />';
             $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
             $message .= "<tr style='background: #eee;'><td><strong>First Name:</strong> </td><td>" . $firstName . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Last Name:</strong> </td><td>" . $lastName . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Package Name :</strong> </td><td>" . strip_tags($packageName) . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Email :</strong> </td><td>" . strip_tags($from_email) . "</td></tr>";
             $message .= "<tr style='background: #eee;'><td><strong>Mobile Number :</strong> </td><td>" .$mobileNumber. "</td></tr>";
             $message .= "</table>";
             $message .= "</body></html>";
             $config['protocol'] = "smtp";
             $config['smtp_host'] = "ssl://smtp.gmail.com";
             $config['smtp_port'] = "465";
             $config['smtp_user'] = smtp_user; 
             $config['smtp_pass'] = smtp_pass;
             $config['newline'] = "\r\n";
            //  Load email library
            $this->load->library('email');
            $config['charset']  = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);
            $this->email->from($from_email, 'Kochin Holidays');
            $this->email->to(to_email);
            $this->email->subject('Package Request Message');
            $this->email->message($message);
         if($this->email->send())
         {
                $result='1';
                echo json_encode($result);
         } 
         else
         {
               $error = '0';
                echo json_encode($error);
         }
      }

}
