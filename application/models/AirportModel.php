<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AirportModel extends CI_Model{
    var $table = 'airport';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_airport(){
        
        $this->db->select('*');
        $this->db->from('airport');
        $this->db->order_by('airport.AirportId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
}

?>