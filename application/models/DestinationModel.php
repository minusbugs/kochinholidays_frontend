<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DestinationModel extends CI_Model{

    var $table = 'destinations';
    function __construct()
    {
       $this->load->database();
    }
    public function get_destinations(){
        
        $this->db->select('*');
        $this->db->from('destinations');
        $this->db->order_by('destinations.CreatedOn','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }

    public function get_destination_id($DestinationId){
        $this->db->select('*');
        $this->db->from('destinations');
        $this->db->where('DestinationId',$DestinationId);
        $query = $this->db->get();
        return $query->row();
    }

}

?>