<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeSliderModel extends CI_Model{

    var $table = 'homeslider';
    function __construct()
    {
       $this->load->database();
    }
    public function get_all_sliders()
    {
        $this->db->select('homeslider.SliderId,homeslider.SliderTitle1,homeslider.SliderTitle2,homeslider.SliderTitle3');
        $this->db->from('homeslider');       
        $this->db->order_by('homeslider.CreatedOn','desc');
        $this->db->limit(4);
        $query = $this->db->get();
        return $query->result_array();      
    }
}
?>