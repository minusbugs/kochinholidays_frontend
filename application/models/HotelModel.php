<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HotelModel extends CI_Model{
    
    var $table = 'hotel';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_hotel(){
        
        $this->db->select('*');
        $this->db->from('hotel');
        $this->db->order_by('hotel.HotelId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
}

?>