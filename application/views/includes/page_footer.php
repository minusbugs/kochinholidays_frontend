<footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Latest Posts</h4>
                                <hr>
                            </div>

                            <ul class="blog-posts">
                                <li>
                                <a href="single.html"><img src="assets/images/uploads/blog_small_01.jpg" alt="" class="img-responsive alignleft"></a>
                                <h3><a href="single.html">Learning Lost Type Patches</a></h3>
                                <p>For the Lost Type online store I a set of 6 embroidered, inspired by motorcycle. […]</p>
                                <small>Sep 25, 2016</small>
                                </li>
                                <li>
                                <a href="single.html"><img src="assets/images/uploads/blog_small_02.jpg" alt="" class="img-responsive alignleft"></a>
                                <h3><a href="single.html">Barney my dearest PAL - WIP</a></h3>
                                <p>The Barney my dearest from AQ Studio! Donec la  elit libero, a pharetra augue. […]</p>
                                <small>Sep 15, 2016</small>
                                </li>
                            </ul><!-- end blog-posts -->
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Quick Links</h4>
                                <hr>
                            </div>

                            <!-- <ul class="twitter-posts"> -->
                               <ul class="twitter-posts">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Partners</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">E-Catalog</a></li>
                                <li><a href="#">Subscribe</a></li>
                            </ul><!-- end list -->
                            
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>About us</h4>
                                <hr>
                            </div>

                            <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum..</p>

                            <ul class="social-links list-inline">
                                <li class="icon facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="icon twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="icon google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="icon instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="icon pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li class="icon youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li class="icon rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul><!-- end social -->
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->


                <div class="row"></div><!-- end row -->
            </div><!-- end container -->
        </footer>

        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; HolydayZone INC. All rights reserved 2017.</p>
                    </div><!-- end col -->

                    <div class="col-md-6 text-right hidden-xs">
                        <ul class="list-inline">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Terms of Usage</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></li>
                        </ul>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </div>
    <!-- end wrapper -->

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/parallax.js"></script>
    <script src="assets/js/animate.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/fitvid.js"></script>
    <!-- FOR SINGLE LISTING PAGES -->
    <script src="assets/js/bootstrap-select.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/price-ranger.js"></script>

    <script>
$(function() {

  // Select2 Box
  $('#select2').select2();

});
</script>