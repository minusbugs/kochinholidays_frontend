<head>
    <title><?php echo $main_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Kochin Holidays is a travel tour operating company with a difference packages. <br>We introduce world class travel packages and services for your luxury and comfort.">
    <meta name="keywords" content="Kerala Tour Package , Package Tour Kerala, Kerala Tourism Package,Kerala Tour Place , Destination Tour Kerala , Kerala Tourism Destination">
    <meta name="author" content="minusbugs PVT LTD">
    <link rel="icon" href="<?php echo site_url();?>fav.ico">
    <link href="<?php echo site_url();?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/animate.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/select2.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/style.css" rel="stylesheet">
     <link href="<?php echo site_url();?>css/main.css" rel="stylesheet">      

    <script src="<?php echo site_url();?>js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
   

    <script src="<?php echo site_url();?>js/jquery-ui.js"></script>
    <script src="<?php echo site_url();?>js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo site_url();?>js/jquery.easing.1.3.js"></script>
    <script src="<?php echo site_url();?>js/superfish.js"></script>

    <script src="<?php echo site_url();?>js/select2.js"></script>

    <script src="<?php echo site_url();?>js/jquery.parallax-1.1.3.resize.js"></script>

    <script src="<?php echo site_url();?>js/SmoothScroll.js"></script>

    <script src="<?php echo site_url();?>js/jquery.appear.js"></script>

    <script src="<?php echo site_url();?>js/jquery.caroufredsel.js"></script>
    <script src="<?php echo site_url();?>js/jquery.touchSwipe.min.js"></script>

    <script src="<?php echo site_url();?>js/jquery.ui.totop.js"></script>

    <script src="<?php echo site_url();?>js/script.js"></script>
     <script src="<?php echo site_url();?>js/main.js"></script>

</head>
<body class="front">
<div class="loader">
  <div class="cssload-container">
    <div class="cssload-speeding-wheel"></div>
  </div>
</div>
<div id="main">

<div class="top1_wrapper">
    <div class="container">
        <div class="top1 clearfix">
            <div class="email1"><a href="#">reservations@kochinholidays.co.in</a></div>
            <div class="phone1">+91-996 161 8320</div>
            <div class="phone1">+91-0484 4050605</div>
            <div class="phone1">+91-974 621 2945</div>
            <div class="social_wrapper">
                <ul class="social clearfix">
                <li><a href="https://facebook.com/Kochin-Holidays-122755211612951/?ref=bookmarks"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                </ul>
            </div>
        
        </div>
    </div>
</div>

<div class="top2_wrapper">
    <div class="container">
        <div class="top2 clearfix">
        <header>
            <div class="logo_wrapper">
            <a href="<?php echo site_url("/"); ?>" class="logo">
                <img src="<?php echo site_url(); ?>images/logo.png" alt="" class="img-responsive">
            </a>
            </div>
        </header>
        <div class="navbar navbar_ navbar-default">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <div class="navbar-collapse navbar-collapse_ collapse">
                <ul class="nav navbar-nav sf-menu clearfix">
                    <li><a href="<?php echo site_url("/"); ?>">Home</a></li>
                    <li class="<?php if($title_name=="aboutus"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/about"); ?>">About Us</a></li>
                    <li class="<?php if($title_name=="packages"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/packages"); ?>">Packages</a></li>
                    <li class="<?php if($title_name=="destination"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/destinations"); ?>">Destination</a></li>
                
                    
                    <li class="<?php if($title_name=="reachus"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/reachus"); ?>">Reach Us</a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- <div class="page_banner"></div> -->

<div class="breadcrumbs1_wrapper">
  <div class="container">
    <div class="breadcrumbs1"><a href="<?php echo site_url("/"); ?>">Home</a><span>/</span><?php echo $page_name; ?></div>
  </div>
</div>


<div id="content">
    <div class="container">
    
          <?php $this->load->view($content); ?>
    </div>
</div>

<div class="bot1_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="logo2_wrapper">
          <a href="<?php echo site_url("/"); ?>" class="logo2">
            <img src="<?php echo site_url(); ?>images/logo2.png" alt="" class="img-responsive">
          </a>
        </div>
        <p>
          Kochin Holidays is a travel tour operating company with a difference. We introduce world class travel packages and services for your luxury and comfort. Our tour package itineraries are hand-picked according to client’s choice and are travelled by our travel representatives. 
        </p>
        <p>
          
        </p>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Quick Links</div>
        <ul class="ul1">
          <li><a href="<?php echo site_url("pages/packages"); ?>">All Packages</a></li>
          <li><a href="<?php echo site_url("pages/destinations"); ?>">All Destinations</a></li>
          
        </ul>

        <div class="social2_wrapper">
          <ul class="social2 clearfix">
            <li class="nav1"><a href="https://facebook.com/Kochin-Holidays-122755211612951/?ref=bookmarks"></a></li>
            <li class="nav2"><a href="#"></a></li>
            <li class="nav3"><a href="#"></a></li>
            <li class="nav4"><a href="#"></a></li>
            <li class="nav5"><a href="#"></a></li>
            <li class="nav6"><a href="#"></a></li>
          </ul>
        </div>

      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Latest Packages</div>
        <div class="twits1">

        <?php 
            $i=0;
            foreach ($packages_details as $packages) { 
                $redirect_url=base_url().'pages/PackageDeatils/'. $packages['PackageId'];
                $package_description=$packages["PackageDescription"];
                $package_description = character_limiter($package_description,80);
        ?>
          <div class="twit1">
            <div class="row">
                <div class="col-sm-3">
                    <a href="<?php echo  $redirect_url;  ?>"><img src="<?php echo package_image_url.$packages['PackageImage']; ?>" alt="<?php echo $packages['PackageTitle']; ?>" class="img-responsive alignleft"></a>
                </div>
                <div class="col-sm-8">
                      <a href="<?php echo  $redirect_url;  ?>"><?php echo $packages['PackageTitle']; ?></a></br><?php echo $package_description; ?> 
                </div>
            </div> 
          </div>
        <?php
            $i=$i+1;
            if($i==2)
                break;
            }
        ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Contact</div>
        
        <div class="">+91-996 161 8320</div>
        <div class="">+91-0484 4050605</div>
        <div class="">+91-974 621 2945</div>
        <div class="support1"><a href="#">reservations@kochinholidays.co.in</a></div>
      </div>
    </div>
  </div>
</div>

<div class="bot2_wrapper">
  <div class="container">
    <div class="left_side">
      KochinHolidays© 2017 <span>|</span>   </span>   <a href="<?php echo site_url("pages/about"); ?>">About Us</a>   <span>|</span>      <a href="<?php echo site_url("pages/reachus"); ?>">Contact Support</a>
    </div>
    <div class="right_side">
      Designed and Developed by <a href="http://minusbugs.com">MinusBugs</a>
    </div>
  </div>
</div>
