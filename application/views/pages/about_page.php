<div id="company1">
  <div class="container">

    <h2 class="animated">ABOUT OUR COMPANY</h2>

    <div class="title1 animated">
    Being an integrated travel and tour operator, we offer a complete range of travel related services for your India holidays. <br>All our staffs are well trained in the hospitality over the years and passionate about their work. They are always eager to meet your requirements 24X7, with impeccably planned trips which reflect our hands-on experience and in-depth researches.
At Kochin Holidays, we understand that each traveler has unique needs and we are ready to walk an extra mile to fulfill them.
    </div>

    <br>

    <div class="row">
      <div class="col-sm-12 col-md-6 col-md-push-6">
        <div class="content">
          <div class="txt1 animated">TRAVEL AGENCY</div>
          <div class="txt2 animated">Kochin Holidays is a travel tour operating company with a difference packages. <br>We introduce world class travel packages and services for your luxury and comfort.</div>




          <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="0">
            <div class="txt">Flights</div>
            <div class="bg">
              <div class="animated-distance" data-num="94" data-duration="1300" data-animation-delay="0"><span></span></div>
            </div>
          </div>

          <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="100">
            <div class="txt">Hotels</div>
            <div class="bg">
              <div class="animated-distance" data-num="87" data-duration="1300" data-animation-delay="100"><span></span></div>
            </div>
          </div>

        


        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-md-pull-6">
        <img src="<?php echo site_url();?>images/company1.png" alt="" class="img1 img-responsive animated">
      </div>
    </div>
  </div>
</div>

<div id="why1">
  <div class="container">

    <h2 class="animated">WHY WE ARE THE BEST</h2>

    <div class="title1 animated">Kochin Holidays is a travel tour operating company with a difference packages. <br>We introduce world class travel packages and services for your luxury and comfort. </div>

    <br><br>

    <div class="row">
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="200">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/destinations"); ?>">
              <figure class="">
                <img src="<?php echo site_url();?>images/why1.png" alt="" class="img1 img-responsive">
                <img src="<?php echo site_url();?>images/why1_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Amazing Travel</div>
                <div class="txt2">We specialize in Honeymoons and Destination weddings-Let your special time be stress free and exciting! </div>
                
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="300">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/packages"); ?>">
              <figure class="">
                <img src="<?php echo site_url();?>images/why2.png" alt="" class="img1 img-responsive">
                <img src="<?php echo site_url();?>images/why2_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Discover</div>
                <div class="txt2">Discover how Kochin Holidays Tour can you help you find everything you want.</div>
                <!-- <div class="txt3">Read More</div> -->
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="400">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/reachus"); ?>">
              <figure class="">
                <img src="<?php echo site_url();?>images/why3.png" alt="" class="img1 img-responsive">
                <img src="<?php echo site_url();?>images/why3_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Book Your Trip</div>
                <div class="txt2">You can easily book your favourite tour packages & Flight form Kochin Holidays .</div>
                
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="500">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/reachus"); ?>">
              <figure class="">
                <img src="<?php echo site_url();?>images/why4.png" alt="" class="img1 img-responsive">
                <img src="<?php echo site_url();?>images/why4_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Nice Support</div>
                <div class="txt2">Let our team of experienced agents save you time, and money while providing you outstanding customer service.</div>
                <!-- <div class="txt3">Read More</div> -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

