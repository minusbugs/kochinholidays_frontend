<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $main_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="description" content="Kochin Holidays is a travel tour operating company with a difference packages. <br>We introduce world class travel packages and services for your luxury and comfort.">
    <meta name="keywords" content="Kerala Tour Package , Package Tour Kerala, Kerala Tourism Package,Kerala Tour Place , Destination Tour Kerala , Kerala Tourism Destination">
    <meta name="author" content="minusbugs PVT LTD">
    <link rel="icon" href="<?php echo site_url();?>fav.ico">
    <link href="<?php echo site_url();?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/animate.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/select2.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/style.css" rel="stylesheet">
    <link href="<?php echo site_url();?>css/main.css" rel="stylesheet">


    
    <script src="<?php echo site_url();?>js/jquery.js"></script>
    <script src="<?php echo site_url();?>js/jquery-ui.js"></script>
    <script src="<?php echo site_url();?>js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo site_url();?>js/jquery.easing.1.3.js"></script>
    <script src="<?php echo site_url();?>js/superfish.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="<?php echo site_url();?>js/select2.js"></script>

    <script src="<?php echo site_url();?>js/jquery.parallax-1.1.3.resize.js"></script>

    <script src="<?php echo site_url();?>js/SmoothScroll.js"></script>

    <script src="<?php echo site_url();?>js/jquery.appear.js"></script>

    <script src="<?php echo site_url();?>js/jquery.caroufredsel.js"></script>
    <script src="<?php echo site_url();?>js/jquery.touchSwipe.min.js"></script>

    <script src="<?php echo site_url();?>js/jquery.ui.totop.js"></script>

    <script src="<?php echo site_url();?>js/script.js"></script>
    <script src="<?php echo site_url();?>js/main.js"></script>

</head>
<body class="front">
<div class="loader">
  <div class="cssload-container">
    <div class="cssload-speeding-wheel"></div>
  </div>
</div>
<div id="ajax-alert"></div>
<div id="main">

<div class="top1_wrapper">
    <div class="container">
        <div class="top1 clearfix">
            <div class="email1"><a href="#">reservations@kochinholidays.co.in</a></div>
            <div class="phone1">+91-996 161 8320</div>
            <div class="phone1">+91-0484 4050605</div>
            <div class="phone1">+91-974 621 2945</div>
            <div class="social_wrapper">
                <ul class="social clearfix">
                <li><a href="https://facebook.com/Kochin-Holidays-122755211612951/?ref=bookmarks"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                </ul>
            </div>
        
        </div>
    </div>
</div>
        
<div class="top2_wrapper">
    <div class="container">
        <div class="top2 clearfix">
        <header>
            <div class="logo_wrapper">
            <a href="#" class="logo">
                <img src="images/logo.png" alt="" class="img-responsive">
            </a>
            </div>
        </header>
        <div class="navbar navbar_ navbar-default">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="navbar-collapse navbar-collapse_ collapse">
               <ul class="nav navbar-nav sf-menu clearfix">
                    <li><a href="<?php echo site_url("/"); ?>">Home</a></li>
                    <li class="<?php if($title_name=="aboutus"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/about"); ?>">About Us</a></li>
                    <li class="<?php if($title_name=="packages"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/packages"); ?>">Packages</a></li>
                    <li class="<?php if($title_name=="destination"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/destinations"); ?>">Destination</a></li>
                    <li class="<?php if($title_name=="reachus"){ echo 'active';} ?>"><a href="<?php echo site_url("pages/reachus"); ?>">Reach Us</a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
</div>
<div id="slider_wrapper">
  <div class="container">
    <div id="slider_inner">
      <div class="">
        <div id="slider">
          <div class="">
            <div class="carousel-box">
              <div class="inner">
                <div class="carousel main">
                  <ul>
                  <?php foreach ($home_slider as $slider) 
                  { ?>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt1"><span><?php echo $slider['SliderTitle1']; ?></span></div>
                          <div class="txt2"><span><?php echo $slider['SliderTitle2']; ?></span></div>
                          <div class="txt3"><span><?php echo $slider['SliderTitle3']; ?></span></div>
                        </div>
                      </div>
                    </li>
                   <?php 
                   } ?>                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="slider_pagination"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="front_tabs">
  <div class="container">
    <div class="tabs_wrapper tabs1_wrapper">
      <div class="tabs tabs1">
        <div class="tabs_tabs tabs1_tabs">

            <ul>
              <li class="active flights"><a href="#tabs-1">Flights</a></li>
              <li class="hotels"><a href="#tabs-2">Hotels</a></li>
              
            </ul>

        </div>
        <div class="tabs_content tabs1_content">

            <div id="tabs-1">
              <form action="<?php base_url()?>Pages/send_flight_mail" id="flightform" method="post" class="form1">
                <div class="row">
                  <div class="col-sm-4 col-md-2">
                    <div class="select1_wrapper">
                      <label>Flying from:</label>
                      <div class="select1_inner">

                        <select name="selectFlightFrom" id="selectFlightFrom"  class="select2 select" style="width: 100%" required>
                          <option value="">City or Airport</option>
                          <?php foreach ($all_airport as $airport) { ?>
                               <option value="<?php echo $airport['AirportName']; ?>"><?php echo $airport['AirportName']; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="select1_wrapper">
                      <label>To:</label>
                      <div class="select1_inner">
                        <select name="selectFlightTo" id="selectFlightTo"  class="select2 select" style="width: 100%" required>
                          <option value="">City or Airport</option>
                           <?php foreach ($all_airport as $airport) { ?>
                               <option value="<?php echo $airport['AirportName']; ?>"><?php echo $airport['AirportName']; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="input1_wrapper">
                      <label>Departing:</label>
                      <div class="input1_inner">
                        <input type="text" name="dateDeparting" id="dateDeparting"  class="input datepicker" placeholder="Mm/Dd/Yy" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="input1_wrapper">
                      <label>Contact No</label>
                      <div class="input2_inner">
                        <input type="text" maxlength="10" name="txtContactNumber" id="txtContactNumber" onblur="return numbervalidate_home_page_flight(this,event);" onkeypress="return numbervalidate_home_page_flight(this,event);"  class="input"  placeholder="Enter your no" required>
                        <span class="help-block" id="home_page_flightnumber" style="color: red;"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-1">
                    <div class="select1_wrapper">
                      <label>Adult:</label>
                      <div class="select1_inner">
                        <select name="selectAdult" id="selectAdult"  class="select2 select select3" style="width: 100%">
                          <!-- <option value="0">0</option> -->
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-1">
                    <div class="select1_wrapper">
                      <label>Child:</label>
                      <div class="select1_inner">
                        <select name="selectChild" id="selectChild"  class="select2 select select3" style="width: 100%">
                          <option value="0">0</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="button1_wrapper">
                      <button type="button" onclick="submitflightmail();"  class="btn-default btn-form1-submit">Send</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div id="tabs-2">
              <form action="<?php base_url()?>Pages/send_hotel_mail" method="post" id="hotelform"  class="form1">
                <div class="row">
                  <div class="col-sm-4 col-md-4">
                    <div class="select1_wrapper">
                      <label>City or Hotel Name:</label>
<!--                       <div class="select1_inner">
                        <select name="selectHotelName" id="selectHotelName" class="select2 select" style="width: 100%" required>
                          <option value=" ">Enter a destination or hotel name</option>
                            <?php //foreach ($all_hotel as $hotel) { ?>
                               <option value="<?php //echo $hotel['HotelName']; ?>"><?php //echo $hotel['HotelName']; ?></option>
                            <?php //} ?>
                        </select>
                      </div> -->
                      <div class="input2_inner">
                        <input type="text" name="selectHotelName" id="selectHotelName" class="input" placeholder="Enter city or hotel name" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="input1_wrapper">
                      <label>Check-In:</label>
                      <div class="input1_inner">
                        <input type="text" name="dateCheckIn" id="dateCheckIn"  class="input datepicker" placeholder="Mm/Dd/Yy" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="input1_wrapper">
                      <label>Check-Out:</label>
                      <div class="input1_inner">
                        <input type="text" name="dateChechOut" id="dateChechOut"  class="input datepicker" placeholder="Mm/Dd/Yy" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="select1_wrapper">
                      <label>Contact No:</label>
                      <div class="input2_inner">
                        <input type="text" maxlength="10" name="txtContactNumber" id="txtContactNumberr" onblur="return numbervalidate_home_page_hotel(this,event);" onkeypress="return numbervalidate_home_page_hotel(this,event);" class="input" value="" placeholder="Enter your no" required>
                        <span class="help-block" id="home_page_hotelnumber" style="color: red;"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-2">
                    <div class="button1_wrapper">
                      <button type="button" onclick="submithotelmail();" class="btn-default btn-form1-submit">Send</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          

        </div>
      </div>
    </div>
  </div>
</div>

<div id="why1">
  <div class="container">

    <h2 class="animated">WHY WE ARE THE BEST</h2>

    <div class="title1 animated">Kochin Holidays is a travel tour operating company with a difference packages. <br>We introduce world class travel packages and services for your luxury and comfort. </div>

    <br><br>

    <div class="row">
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="200">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/destinations"); ?>">
              <figure class="">
                <img src="images/why1.png" alt="" class="img1 img-responsive">
                <img src="images/why1_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Amazing Travel</div>
                <div class="txt2">We specialize in Honeymoons and Destination weddings-Let your special time be stress free and exciting! </div>
                
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="300">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/packages"); ?>">
              <figure class="">
                <img src="images/why2.png" alt="" class="img1 img-responsive">
                <img src="images/why2_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Discover</div>
                <div class="txt2">Discover how Kochin Holidays Tour can you help you find everything you want.</div>
                <!-- <div class="txt3">Read More</div> -->
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="400">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/reachus"); ?>">
              <figure class="">
                <img src="images/why3.png" alt="" class="img1 img-responsive">
                <img src="images/why3_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Book Your Trip</div>
                <div class="txt2">You can easily book your favourite tour packages & Flight form Kochin Holidays .</div>
                
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="500">
          <div class="thumbnail clearfix">
            <a href="<?php echo site_url("pages/reachus"); ?>">
              <figure class="">
                <img src="images/why4.png" alt="" class="img1 img-responsive">
                <img src="images/why4_hover.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Nice Support</div>
                <div class="txt2">Let our team of experienced agents save you time, and money while providing you outstanding customer service.</div>
                <!-- <div class="txt3">Read More</div> -->
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="popular_cruises1">
  <div class="container">

    <h2 class="animated">POPULAR PACKAGES</h2>

    <div class="title1 animated">Explore the most popular Packages.</div>

    <br><br>

    <div id="popular_wrapper" >
      <div id="popular_inner">
        <div class="">
          <div id="popular">
            <div class="">
              <div class="carousel-box">
                <div class="inner">
                  <div class="carousel main">
                    <ul>
                        <?php foreach ($packages_details as $packages) {  
                              $redirect_url=base_url().'pages/PackageDeatils/'. $packages['PackageId'];
                              $package_description=$packages["PackageDescription"];
                              $package_description = character_limiter($package_description,80);
                        ?>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="<?php echo package_image_url.$packages['PackageImage']; ?>" alt="<?php echo $packages['PackageTitle']; ?>" class="img-responsive">
                              <div class="over">
                                <div class="v1"><?php echo $packages['PackageTitle']; ?> </span></div>
                                <!-- <div class="v2">Lorem ipsum dolor sit amet, concateus.</div> -->
                                
                              </div>
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span><?php echo $packages['PackageTitle']; ?></div>
                               <div class="txt2"><?php echo $package_description; ?></div> 
                              <div class="txt3 clearfix">
                                <div class="left_side"><a href="<?php echo site_url("pages/packages"); ?>" class="btn-default btn1">See All</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    <?php
                        }
                    ?> 
                        
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="popular_pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="happy1">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6 col-md-push-6">
        <div class="content">
          <div class="txt1 animated" data-animation="fadeIn" data-animation-delay="100">HAPPY CUSTOMERS</div>
          <div class="txt2 animated" data-animation="fadeIn" data-animation-delay="150">Customer reviews and support </div>
          <div class="txt3 animated" data-animation="fadeIn" data-animation-delay="200">
            <p>
             I had planned my honeymoon to Kerala through Kochin Holidays. I am delighted to have such amazing services at better quotes than competitors. Special thanks to Anwar and his team from Kochin Holidays. I will be loyal to this travel agent !!!<br>
             <a href="#">@sid</a>
            </p>
            <p>
              The travel arrangement was fantastic. I would recommend everyone to use Kochin Holidays. Amazing experience.. Cheers Anwar!!!<br>
               <a href="#">@marwan</a>

            </p>
          </div>

          <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="0">
            <div class="txt">Flights</div>
            <div class="bg">
              <div class="animated-distance" data-num="94" data-duration="1300" data-animation-delay="0"><span></span></div>
            </div>
          </div>

          <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="100">
            <div class="txt">Hotels</div>
            <div class="bg">
              <div class="animated-distance" data-num="87" data-duration="1300" data-animation-delay="100"><span></span></div>
            </div>
          </div>

          

        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-md-pull-6 animated" data-animation="fadeInLeft" data-animation-delay="200">
        <img src="images/people.png" alt="" class="img1 img-responsive">
      </div>
    </div>
  </div>
</div>


<div id="partners">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner1.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner1_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner2.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner2_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner3.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner3_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner4.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner4_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner5.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner5_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="images/partners/partner6.jpg" alt="" class="img1 img-responsive">
                <img src="images/partners/partner6_hover.jpg" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<div class="bot1_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="logo2_wrapper">
          <a href="#" class="logo2">
            <img src="images/logo2.png" alt="" class="img-responsive">
          </a>
        </div>
        <p>
          Kochin Holidays is a travel tour operating company with a difference. We introduce world class travel packages and services for your luxury and comfort. Our tour package itineraries are hand-picked according to client’s choice and are travelled by our travel representatives. 
        </p>
        <p>
          
        </p>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Quick Links</div>
        <ul class="ul1">
          <li><a href="<?php echo site_url("pages/packages"); ?>">All Packages</a></li>
          <li><a href="<?php echo site_url("pages/destinations"); ?>">All Destinations</a></li>
          
        </ul>

        <div class="social2_wrapper">
          <ul class="social2 clearfix">
            <li class="nav1"><a href="https://facebook.com/Kochin-Holidays-122755211612951/?ref=bookmarks"></a></li>
            <li class="nav2"><a href="#"></a></li>
            <li class="nav3"><a href="#"></a></li>
            <li class="nav4"><a href="#"></a></li>
            <li class="nav5"><a href="#"></a></li>
            <li class="nav6"><a href="#"></a></li>
          </ul>
        </div>

      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Latest Packages</div>
        <div class="twits1">

        <?php 
            $i=0;
            foreach ($packages_details as $packages) { 
                $redirect_url=base_url().'pages/PackageDeatils/'. $packages['PackageId'];
                $package_description=$packages["PackageDescription"];
                $package_description = character_limiter($package_description,80);
        ?>
          <div class="twit1">
            <div class="row">
                <div class="col-sm-3">
                    <a href="<?php echo  $redirect_url;  ?>"><img src="<?php echo package_image_url.$packages['PackageImage']; ?>" alt="<?php echo $packages['PackageTitle']; ?>" class="img-responsive alignleft"></a>
                </div>
                <div class="col-sm-8">
                      <a href="<?php echo  $redirect_url;  ?>"><?php echo $packages['PackageTitle']; ?></a></br><?php echo $package_description; ?> 
                </div>
            </div> 
          </div>
        <?php
            $i=$i+1;
            if($i==2)
                break;
            }
        ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Contact</div>
        
        <div class="">+91-996 161 8320</div>
        <div class="">+91-0484 4050605</div>
        <div class="">+91-974 621 2945</div>
        <div class="support1"><a href="#">reservations@kochinholidays.co.in</a></div>
      </div>
    </div>
  </div>
</div>

<div class="bot2_wrapper">
  <div class="container">
    <div class="left_side">
      KeralaHolidays © 2017   <span>|</span>    <a href="<?php echo site_url("pages/about"); ?>">About Us</a>   <span>|</span>      <a href="<?php echo site_url("pages/reachus"); ?>">Contact Support</a>
    </div>
    <div class="right_side">
      Designed and Developed by <a href="http://minusbugs.com">MinusBugs</a>
    </div>
    
  </div>
</div>