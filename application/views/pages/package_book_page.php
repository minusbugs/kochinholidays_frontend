 <?php 
$attributes = array('id' => 'packagedetailsform','action' => ' ','class' => 'form-horizontal','enctype'=>'multipart/form-data');

echo form_open('Pages/send_Package_Details',$attributes);

?>
<div id="ajax-alert"></div>
<div class="row">
    <div class="col-sm-12">
        <h3 class="text-center hch2"><?php echo $packages_details_by_id->PackageTitle; ?></h3>
        <input type="hidden" name="hd_packagename" value="<?php echo $packages_details_by_id->PackageTitle; ?>">
        <div class="clearfix"></div>
        <p class="address text-center"><?php echo $packages_details_by_id->PackageDescription; ?></p>
    
        <div class="clearfix"></div>

        <div class="col-md-6 booking-row">
            <h3 class="line">TRAVELLER INFORMATION</h3>
            <div class="input2_wrapper">
                <label class="col-md-5" style="padding-left:0;padding-top:12px;">First Name</label>
                <div class="col-md-7" style="padding-right:0;padding-left:0;">
                    <input type="text" maxlength="40" class="form-control" name="txtFirtsName" id="txtFirtsName" onblur="validateFirstName_package_book_page(this)" onkeypress="validateFirstName_package_book_page(this)" placeholder="Your First Name" spellcheck="false" required>
                    <span class="help-block" id="package_book_pagefirstname" style="color: red;"></span>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="input2_wrapper">
                <label class="col-md-5" style="padding-left:0;padding-top:12px;">Last Name</label>
                <div class="col-md-7" style="padding-right:0;padding-left:0;">
                    <input type="text" maxlength="40" class="form-control" name="txtLastName" id="txtLastName"  placeholder="Your Last Name" spellcheck="false" required>
                    <span class="help-block" id="package_book_pagelastname" style="color: red;"></span>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="input2_wrapper">
                <label class="col-md-5" style="padding-left:0;padding-top:12px;">Your Email</label>
                <div class="col-md-7" style="padding-right:0;padding-left:0;">
                    <input type="email" class="form-control" name="txtEmail" id="txtEmail" onblur="validateEmail_package_book_page(this)" onkeypress="validateEmail_package_book_page(this)" placeholder="your@email.com" spellcheck="false" required>
                    <span class="help-block" id="package_book_pageemail" style="color: red;"></span>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="input2_wrapper">
                <label class="col-md-5" style="padding-left:0;padding-top:12px;">Your Mobile No</label>
                <div class="col-md-7" style="padding-right:0;padding-left:0;">
                    <input type="text" maxlength="10" class="form-control" name="txtMobileNo" id="txtMobileNo" onblur="return numbervalidate_package_book_page(this,event);" onkeypress="return numbervalidate_package_book_page(this,event);" placeholder="Your Mobile No" required>
                    <span class="help-block" id="package_book_pagenumber" style="color: red;"></span>
                </div>
            </div>
            
            <div class="clearfix"></div>
            <div class="margin-top"></div>
            <div class="left_side">
                 <button type="button" class="btn btn-default btn-cf-submit3" onclick="submitPackageDetails();"  >RESERVE NOW</button>  
            </div>
           

        </div>
        
        <div class="col-md-2"></div>
        <div class="col-md-4 booking-row">
            <h3 class="line">Gallery</h3>
            <img src="<?php echo package_image_url.$packages_details_by_id->PackageImage; ?>" alt="" class="img-responsive">
        </div>
        
    </div>
</div>
