 <?php 
$attributes = array('id' => 'contactform','action' => ' ','class' => 'form-horizontal','enctype'=>'multipart/form-data');

echo form_open('Pages/send_mail',$attributes);

?>
<div id="ajax-alert"></div>
<div class="row">
      <div class="col-sm-6">

        <h3>CONTACT INFO</h3>

        
        <br>

        <h4>ADDRESS</h4>
        <p>
          Kochin Holidays<br>
          Reg No:01/1140214023239/6120c<br>
          1st Floor , Rose Villa ,Alappat Cross Road<br>
          Ravipuram , Cochin -16<br>
          <a href="#">reservations@kochinholidays.co.in</a>
        </p>

        <h4>PHONE</h4>

        <p>
          +91 0484 4050605<br>
          +996 161 8320
        </p>

        <div class="social3_wrapper">
          <ul class="social3 clearfix">
            <li><a href="https://facebook.com/Kochin-Holidays-122755211612951/?ref=bookmarks"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
          </ul>
        </div>


      </div>
      <div class="col-sm-6">

        <h3>CONTACT FORM</h3>

        <div id="note"></div>
        <div id="fields">
         
            
            <div class="form-group">
                <label for="inputName">Your Name</label>
                <input type="text" maxlength="40" id="fullname" class="form-control"  name="name" placeholder="Full Name" onblur="validateFullName_reachus_page(this)" onkeypress="validateFullName_reachus_page(this)" required>
                <span class="help-block" id="reachus_pagefullname" style="color: red;"></span>
            </div>

            <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" id="email" onblur="validateEmail_reachus_page(this)" onkeypress="validateEmail_reachus_page(this)" class="form-control" placeholder="your@email.com"  name="email" required>
                <span class="help-block" id="reachus_pageemail" style="color: red;"></span>
            </div>


            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                    <label for="inputMessage">Your Message</label>
                    <textarea id="message" maxlength="250"  onblur="validateMessage_reachus_page(this)" onkeypress="validateMessage_reachus_page(this)" class="form-control" rows="7"  name="content" placeholder="Message" required></textarea>
                    <span class="help-block" id="reachus_pagemessage" style="color: red;"></span>
                </div>
              </div>
            </div>
            <button onclick="submitcontactmail();" type="button" class="btn-default btn-cf-submit">send message</button>
        </div>


      </div>
    </div>
