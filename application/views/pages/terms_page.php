<section class="section lb page-title little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix">
                    <div class="pull-left">
                        <h3>Terms Of Usage </h3>
                        
                    </div>

                    <div class="pull-right hidden-xs">
                        <ul class="breadcrumb">
                            <li><a href="javascript:void(0)">Home</a></li>
                            <li class="active">Terms Of Usage</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-meta">
                    <h3>Inclusions</h3>
                    <ul>
                        <li>Above mentioned accommodations are on twin sharing basis.</li>
                        <li>Transportation as mentioned with an English / Hindi speaking friendly driver cum guide throughout your trip.</li>
                        <li> Above mentioned accommodation includes EP / CP Meal plans</li>
                        <li>If Houseboat is included in your package its exclusively for your family with all meals which includes Welcome Drink, Snacks, Tea, Lunch, Dinner, Breakfast, Mineral water etc.
                        Child below 5 yrs will be complimentarily.</li>
                        <li> Driver’s Bata, Toll, Parking, Road tax, Fuel charge, Interstate charges.</li>
                        <li>Sightseeing will be applicable only mentioned in the itinerary as per your arrival time. (Covering sightseeing depends upon your time management. you can also manage the time by discussion with the driver).</li>
                        <li>During off season, if available (A/C Indica) will be upgraded to any one of these vehicle – Indigo or Verito or Logan or Etios or Dzire.</li>
                    </ul>

                     <h3>Exclusions</h3>
                    <ul>
                        <li>Entrance fees to Amusement parks, Elephant ride & Boating charges & forest entrance charge in Thekkady (pay directly).</li>
                        <li>Any portage at airports and hotels, tips, insurance, wine, mineral water, telephone charges, and all items of personal nature.</li>
                        <li>Optional activities mentioned in the itinerary.</li>
                        <li>Any services not specifically mentioned in the inclusions.</li>
                        <li>If the package is for two adults, Extra person/children will not be allowed.</li>
                        <li>Mandatory Gala Dinner on Xmas & New year Eve (Pay Directly) as per hotel policy.</li>
                       
                    </ul>
                    
                  
                </div>
            </div>
              <div class="clearfix"></div>
        </div>
    </div>
</section>
  