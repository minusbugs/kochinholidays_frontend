-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2017 at 08:15 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `holidayzonedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_db`
--

CREATE TABLE `admin_db` (
  `AdminId` int(11) NOT NULL,
  `AdminName` varchar(255) NOT NULL,
  `AdminPassword` varchar(255) NOT NULL,
  `UserType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_db`
--

INSERT INTO `admin_db` (`AdminId`, `AdminName`, `AdminPassword`, `UserType`) VALUES
(1, 'admin', 'ADMIN', 'Admin'),
(2, 'anglin', 'anglin123', 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `airport`
--

CREATE TABLE `airport` (
  `AirportId` int(11) NOT NULL,
  `AirportName` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airport`
--

INSERT INTO `airport` (`AirportId`, `AirportName`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'abudabi', '2017-08-21 10:11:46', '2017-08-21 10:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `DestinationId` int(11) NOT NULL,
  `DestinationName` varchar(100) NOT NULL,
  `DestinationImage` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`DestinationId`, `DestinationName`, `DestinationImage`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'kulu', '1503987846.jpg', '2017-08-29 06:24:06', '2017-08-29 06:24:06'),
(2, 'manaly', '1503987854.jpg', '2017-08-29 06:24:15', '2017-08-29 06:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `flight`
--

CREATE TABLE `flight` (
  `FlightId` int(11) NOT NULL,
  `FlightName` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `homeslider`
--

CREATE TABLE `homeslider` (
  `SliderId` int(11) NOT NULL,
  `SliderTitle1` varchar(250) NOT NULL,
  `SliderTitle2` varchar(250) NOT NULL,
  `SliderTitle3` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeslider`
--

INSERT INTO `homeslider` (`SliderId`, `SliderTitle1`, `SliderTitle2`, `SliderTitle3`, `CreatedOn`, `UpdatedOn`) VALUES
(2, '5 Days', 'Bangalore', 'Good Trip', '2017-08-29 09:30:17', '2017-08-29 09:30:17'),
(4, '3 Days', 'Hydrabad Trip', 'we give you a grand trip', '2017-08-29 11:32:02', '2017-08-29 11:32:02'),
(5, '3 Days', 'Kochin to Delhi', 'expertised guiding with us', '2017-08-30 05:53:39', '2017-08-30 05:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `HotelId` int(11) NOT NULL,
  `HotelName` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`HotelId`, `HotelName`, `CreatedOn`, `UpdatedOn`) VALUES
(3, 'goos', '2017-08-14 10:42:05', '2017-08-14 10:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `PackageId` int(11) NOT NULL,
  `PackageTitle` varchar(500) NOT NULL,
  `PackageDescription` varchar(1000) NOT NULL,
  `PackageImage` varchar(100) NOT NULL,
  `PackageAmount` float NOT NULL,
  `DestinationId` int(11) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`PackageId`, `PackageTitle`, `PackageDescription`, `PackageImage`, `PackageAmount`, `DestinationId`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'manaly-kulu', 'good and finest', '1503988412.jpg', 0, 1, '2017-08-29 06:33:32', '2017-08-29 06:33:32'),
(2, 'kovalam', 'sd dsedf dfrg', '1503988432.jpg', 0, 2, '2017-08-29 06:33:52', '2017-08-29 06:33:52'),
(3, 'winter', 'sf wt ert', '1503988464.jpg', 0, 2, '2017-08-29 06:34:24', '2017-08-29 06:34:24'),
(4, 'rthg rth ', 'trh rth ', '1503988478.jpg', 0, 2, '2017-08-29 06:34:38', '2017-08-29 06:34:38'),
(5, 'esf ', ' wetr', '1503991829.jpg', 0, 2, '2017-08-29 07:30:29', '2017-08-29 07:30:29'),
(6, 'ghj', 'gjghjgh tyj ', '1503992428.jpg', 0, 2, '2017-08-29 07:40:28', '2017-08-29 07:40:28'),
(7, 'winter', 'hujk, yfy r5y m', '1503992447.jpg', 0, 2, '2017-08-29 07:40:47', '2017-08-29 07:40:47'),
(8, 'new', 'good', '1503994875.jpg', 0, 2, '2017-08-29 08:21:15', '2017-08-29 08:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `package_images`
--

CREATE TABLE `package_images` (
  `ImageId` int(11) NOT NULL,
  `PackageId` int(11) NOT NULL,
  `ImageName` varchar(250) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_db`
--
ALTER TABLE `admin_db`
  ADD PRIMARY KEY (`AdminId`);

--
-- Indexes for table `airport`
--
ALTER TABLE `airport`
  ADD PRIMARY KEY (`AirportId`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`DestinationId`);

--
-- Indexes for table `flight`
--
ALTER TABLE `flight`
  ADD PRIMARY KEY (`FlightId`);

--
-- Indexes for table `homeslider`
--
ALTER TABLE `homeslider`
  ADD PRIMARY KEY (`SliderId`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`HotelId`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`PackageId`);

--
-- Indexes for table `package_images`
--
ALTER TABLE `package_images`
  ADD PRIMARY KEY (`ImageId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_db`
--
ALTER TABLE `admin_db`
  MODIFY `AdminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `airport`
--
ALTER TABLE `airport`
  MODIFY `AirportId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `DestinationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `flight`
--
ALTER TABLE `flight`
  MODIFY `FlightId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `homeslider`
--
ALTER TABLE `homeslider`
  MODIFY `SliderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `HotelId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `PackageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `package_images`
--
ALTER TABLE `package_images`
  MODIFY `ImageId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
