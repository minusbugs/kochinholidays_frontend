 function submitcontactmail()
{
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var d = new FormData($("#contactform")[0]);
  var email=$('#email').val();
  var fullname=$('#fullname').val();
  var message=$('#message').val();
  var a='0';
  if($('#fullname').val() == "" || $('#email').val() == ""|| $('#message').val() == "")
  {
    $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
    $("#ajax-alert").alert();
    $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
  }
  else
  {
    if (!(reg.test(email))) 
      {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid email address");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
    else if (fullname.length < 2) 
      {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 2 letters in Full Name");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
    else if (message.length < 9) 
      {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 10 letters in Message");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
    else
    {
      $('.loader').show(); 
      $('html').addClass('.noscroll');
    $.ajax({
             type: "POST",
            data: d,
            url: "../Pages/send_mail",
            dataType: 'json',

            processData: false,
            contentType: false,
            success: function (result) {
              var a = JSON.parse(result);              
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll '); 
              if (a == '1') 
             {
                $("#ajax-alert").addClass("alert  alert-success");                        
                $("#ajax-alert").text("Mail Has been Send");
                $("#ajax-alert").alert();
                $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
                $('#fullname').val('');
                $("#email").val(' ');
                $("#message").val(' ');
              }
              else
               {
               alert("Sending Failed");
              }
            },
              error: function() 
              {
                $('.loader').hide();
                $('html').removeClass('.noscroll '); 
                $('body').addClass('.scroll ');  
                alert('Network Error');
              }
          })
      }
  }
}
function submitPackageDetails()
{
  var filter = /^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})$/;
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var contact = $('#txtMobileNo').val();
  var email=$('#txtEmail').val();
  var firstname=$('#txtFirtsName').val();
  var lastname=$('#txtLastName').val();
  var d = new FormData($("#packagedetailsform")[0]);
  var a='0';
  if($('#txtFirtsName').val() == "" || $('#txtLastName').val() == ""|| $('#txtEmail').val() == "" || $('#txtMobileNo').val() == "")
  {
    $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
    $("#ajax-alert").alert();
    $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
  }
  else
  {
     if (!(filter.test(contact))) 
     {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid mobile number");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
      else if (!(reg.test(email))) 
      {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid email address");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
      else if (firstname.length < 2) 
      {
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 2 letters in First Name");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      }
      // else if (lastname.length < 2) 
      // {
      //   $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 3 letters in Last Name");
      //   $("#ajax-alert").alert();
      //   $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){}); 
      // }
      else
      {
        $('.loader').show(); 
        $('html').addClass('.noscroll');
        $.ajax({
              type: "POST",
              data: d,
              url: "../send_Package_Details",
              dataType: 'json',

              processData: false,
              contentType: false,
              success: function (result) {
                var a = JSON.parse(result);
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll '); 
                if (a == '1') 
               {

                      $("#ajax-alert").addClass("alert  alert-success");
                      $("#ajax-alert").text("Package Request Has been Send");
                      $("#ajax-alert").alert();
                      $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
                    $('#txtFirtsName').val('');
                    $("#txtLastName").val(' ');
                    $("#txtEmail").val(' ');
                    $("#txtMobileNo").val(' ');
                    window.location.href = "../../Pages/packages";
                }
                else
                 {
                 alert("Sending Failed");
                }
              },
                error: function() 
                {
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll ');  
                  alert('Network Error');
                }
          })        
      }
  }
}

function submitflightmail()
{
    var formData = new FormData($("#flightform")[0]);
    var contact = $('#txtContactNumber').val();
        var a='0';
       if($('#selectFlightFrom').val() == ""|| $('#selectFlightFrom').val() == null || $('#selectFlightTo').val() == ""||$('#selectFlightTo').val() == null || $('#dateDeparting').val() == "" || $('#txtContactNumber').val() == "" || $('#selectAdult').val() == ""||$('#selectAdult').val() == null || $('#selectChild').val() == ""|| $('#selectChild').val() == null)
        {
          $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
          $("#ajax-alert").alert();
          $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
          var filter = /^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})$/;  
          if (filter.test(contact)) 
           {  
             $('.loader').show(); 
             $('html').addClass('.noscroll');    
             formData.append("FlightFrom", $("#selectFlightFrom").val());
             formData.append("FlightTo", $("#selectFlightTo").val());
             formData.append("Departing", $("#dateDeparting").val());
             formData.append("ContactNumber", $("#txtContactNumber").val());
             formData.append("Adult", $("#selectAdult").val());
             formData.append("Child", $("#selectChild").val());
          $.ajax({
                type: "POST",
                data: formData,
                url: "Pages/send_flight_mail",
                dataType: 'json',

                processData: false,
                contentType: false,
                success: function (result) {
                  var a = JSON.parse(result);
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll ');  
                  if (a == '1') 
                 {
                    $("#ajax-alert").addClass("alert  alert-success");
                    $("#ajax-alert").text("Flight Request Has been Send");
                    $("#ajax-alert").alert();
                    $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
                    $('#selectFlightFrom').select2('val','');
                    $("#selectFlightTo").select2('val','');
                    $("#dateDeparting").val(' ');
                    $("#txtContactNumber").val(' ');
                    $("#selectAdult").select2('val','');
                    $("#selectChild").select2('val','');
                  }
                  else
                   {
                   alert("Sending Failed");
                  }
                },
                error: function() 
                {
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll ');  
                  alert('Network Error');
                }
              })
            }
            else
            {
              $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid mobile number");
              $("#ajax-alert").alert();
              $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});         
            }
   }
}
function submithotelmail()
{
    var contact = $('#txtContactNumberr').val();
    var filter = /^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})$/;  
    var formData = new FormData($("#hotelform")[0]);
        var a='0';
        if($('#selectHotelName').val() == "" || $('#dateCheckIn').val() == ""|| $('#dateChechOut').val() == "" || $('#txtContactNumberr').val() == "")
        {
                        $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
           if (filter.test(contact)) 
           { 
             formData.append("HotelName", $("#selectHotelName").val());
             formData.append("CheckIn", $("#dateCheckIn").val());
             formData.append("ChechOut", $("#dateChechOut").val());
             formData.append("ContactNumber", $("#txtContactNumberr").val());
             $('.loader').show(); 
             $('html').addClass('.noscroll');    
          $.ajax({
                type: "POST",
                data: formData,
                url: "Pages/send_hotel_mail",
                dataType: 'json',

                processData: false,
                contentType: false,
                success: function (result) {
                  var a = JSON.parse(result);
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll ');
                  if (a == '1') 
                 {
                        $("#ajax-alert").addClass("alert  alert-success");
                        $("#ajax-alert").text("Hotel Request Has been Send");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
                      $('#selectHotelName').val('');
                      $("#dateCheckIn").val(' ');
                      $("#dateChechOut").val(' ');
                      $("#txtContactNumberr").val(' ');
                  }
                  else
                   {
                   alert("Sending Failed");
                  }
                },
                error: function() 
                {
                  $('.loader').hide();
                  $('html').removeClass('.noscroll '); 
                  $('body').addClass('.scroll ');  
                  alert('Network Error');
                }
              })
            }
            else
            {
              $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid mobile number");
              $("#ajax-alert").alert();
              $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});         
            }
   }
}

//mobile number validation of package_book_page
function numbervalidate_package_book_page(textbox, e) 
{

  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57)) {
    document.getElementById("package_book_pagenumber").innerHTML = "Numbers only";
    $("#txtMobileNo").css({
      "border": "1px solid red",
    });
    return false;
  } else {
    document.getElementById("package_book_pagenumber").innerHTML = "";
    $("#txtMobileNo").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

//email validation of package_book_page
function validateEmail_package_book_page(emailField) 
{
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    document.getElementById("package_book_pageemail").innerHTML = "Enter valid email Id";
    $("#txtEmail").css({
      "border": "1px solid red",
    });
    return false;
  }
  if (reg.test(emailField.value) == true) {
    document.getElementById("package_book_pageemail").innerHTML = "";
    $("#txtEmail").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

//email validation of reachus_page
function validateEmail_reachus_page(emailField) 
{
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    document.getElementById("reachus_pageemail").innerHTML = "Enter valid email Id";
    $("#email").css({
      "border": "1px solid red",
    });
     return false;
  }
  if (reg.test(emailField.value) == true) {
    document.getElementById("reachus_pageemail").innerHTML = "";
    $("#email").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//full name validation of reachus_page
function validateFullName_reachus_page(fullnameField) 
{
  if (fullnameField.value.length < 2) {
    document.getElementById("reachus_pagefullname").innerHTML = "Enter at least 2 letters";
    $("#fullname").css({
      "border": "1px solid red",
    });
     return false;
  }
  else {
    document.getElementById("reachus_pagefullname").innerHTML = "";
    $("#fullname").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//full name validation of reachus_page
function validateMessage_reachus_page(messageField) 
{
  if (messageField.value.length < 9) {
    document.getElementById("reachus_pagemessage").innerHTML = "Enter at least 10 letters";
    $("#message").css({
      "border": "1px solid red",
    });
    return false;
  }
  else {
    document.getElementById("reachus_pagemessage").innerHTML = "";
    $("#message").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//mobile number validation of home_page_flightnumber
function numbervalidate_home_page_flight(textbox, e) 
{

  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57)) {
    document.getElementById("home_page_flightnumber").innerHTML = "Numbers only";
    $("#txtContactNumber").css({
      "border": "1px solid red",
    });
    return false;
  } else {
    document.getElementById("home_page_flightnumber").innerHTML = "";
    $("#txtContactNumber").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

//mobile number validation of home_page_hotelnumber
function numbervalidate_home_page_hotel(textbox, e) 
{

  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57)) {
    document.getElementById("home_page_hotelnumber").innerHTML = "Numbers only";
    $("#txtContactNumberr").css({
      "border": "1px solid red",
    });
    return false;
  } else {
    document.getElementById("home_page_hotelnumber").innerHTML = "";
    $("#txtContactNumberr").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

//first name validation of package_book_page
function validateFirstName_package_book_page(firstnameField) 
{
  if (firstnameField.value.length < 2) {
    document.getElementById("package_book_pagefirstname").innerHTML = "Enter at least 2 letters";
    $("#txtFirtsName").css({
      "border": "1px solid red",
    });
    return false;
  }
  else {
    document.getElementById("package_book_pagefirstname").innerHTML = "";
    $("#txtFirtsName").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//last name validation of package_book_page
// function validateLastName_package_book_page(lastnameField) 
// {
//   if (lastnameField.value.length < 2) {
//     document.getElementById("package_book_pagelastname").innerHTML = "Enter at least 3 letters";
//     $("#txtLastName").css({
//       "border": "1px solid red",
//     });
//     return false;
//   }
//   else {
//     document.getElementById("package_book_pagelastname").innerHTML = "";
//     $("#txtLastName").css({
//       "border": "1px solid #ccc",

//     });
//     return true;
//   }
// }
//end

//home page date picker validation
$(function() {
    $( "#dateDeparting" ).datepicker({ minDate: 0});
  });

$(function() {
$("#dateCheckIn").datepicker({
  minDate: 0,
  onSelect: function(date) {
    $("#dateChechOut").datepicker('option', 'minDate', date);
  }
});
$("#dateChechOut").datepicker({ minDate: 0});
 });
//end

//home page SELECT FROM SELECT TO validation
$(function() {
  var $dropdown1 = $("select[name='selectFlightFrom']");
  var $dropdown2 = $("select[name='selectFlightTo']");

  $dropdown1.change(function() {
      var selecteditem2 = document.getElementById("selectFlightTo").value;
      $dropdown2.empty().append($dropdown1.find('option').clone());
      var selectedItem = $(this).val();
      if(selectedItem == selecteditem2)
      {
        $("#selectFlightTo").select2('val','');
      }
      if(selectedItem)
      {
        $dropdown2.find('option[value="' + selectedItem + '"]').remove();
      }
  });
});
//end